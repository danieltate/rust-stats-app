(function(){
    'use strict';

    // Declare app level module which depends on views, and components
    angular.module('myApp', [
        'ngRoute',
        'myApp.view1',
        'myApp.view2',
        'myApp.version'
    ]).constant('CFG', {
        env: 'dev',
        resource: function(env) {
            var o = {};
            if(env === 'dev') {
                o.all = 'http://localhost:8000/app/data/fake.json';
                o.query = 'http://localhost:8000/app/data/fakePlayer.json';
            } else {
                o.all = 'http://203.213.96.84:8888/getPlayersGlobalStats';
                o.query = 'http://203.213.96.84:8888/getPlayerKillsHistory?ids=';
            }
            return o;
        } 
    }).config(['$routeProvider', function($routeProvider) {
        $routeProvider.otherwise({redirectTo: '/view1'});
    }]);
})();
