(function(){
'use strict';

angular.module('myApp.view2', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view2', {
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl'
  });
}])

.controller('View2Ctrl', ['$scope', '$http', '$location', function($scope, $http, $location) {


    // Only getting first ID for now
    var idLocal =  $location.url().indexOf('?') + 1;
    var id = $location.url().substr(idLocal, $location.url().length).split(',')[0];

    var local = false;
    var queryPlayer = 'data/fakePlayer.json';
    var queryAll = 'data/fake.json';

    if(!local){
        queryPlayer = 'http://203.213.96.84:8888/getPlayerKillsHistory?ids='+id;
        queryAll = 'http://203.213.96.84:8888/getPlayersGlobalStats';
    }

    var searchString = '//Players[idPayer='+id+']';

    $http.jsonp(queryPlayer).success(function(data){

        var playerIDs = [];
        _.each(data.PlayersKillsHistory, function(data) {
            for(var key in data){
                playerIDs.push(key);
            }
        });

        $http.jsonp(queryAll).success(function(data){

            var found = JSON.search(data, searchString);
            _.each(found, function(person) {
                var total = 0;
                for(var key in person){
                    if(key != 'playerName' && key != 'idPayer'){
                        var value = person[key];
                        total += parseInt(value);
                    }
                }
                person.total = total;
            });
            $scope.playerList = found;
        });

        //$scope.players = data.Players;
    });
}]);
})();
