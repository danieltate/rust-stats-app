(function(){
    'use strict';

    angular.module('myApp.view1', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
      $routeProvider.when('/view1', {
        templateUrl: 'view1/view1.html',
        controller: 'View1Ctrl'
      });
    }])

    .controller('View1Ctrl', ['$scope', '$http', 'CFG', function($scope, $http, CFG) {

        var url = CFG.resource().all;
        $scope.players = [];

        // Assigning something to the scope makes it accessable in the template.
        oboe({
            url: url,
            headers:{'Access-Control-Allow-Origin':'*'},
            async: false,
        }).done(function(data){
            $scope.$apply(function(){
                $scope.players = data.Players;
            });
        });
    }]);
})();
